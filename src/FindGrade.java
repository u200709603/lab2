import java.util.*;
public class Main {

    public static void main(String[] args) {
        int score=-5;
        if (100>=score && score>90) System.out.println('A');
        if (90>score && score>=80) System.out.println('B');
        if (80>score && score>=70) System.out.println('C');
        if (70>score && score>=60) System.out.println('D');
        if (60>score && score>=0) System.out.println('F');
        if (0>score || score>100) System.out.println("It is not a valid score!");

    }
}
